import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyContactCardComponent } from '../my-contact-card/my-contact-card.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports:[
    MyContactCardComponent
  ]
})
export class MyFirstModuleModule { }
