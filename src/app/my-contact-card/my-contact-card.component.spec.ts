import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyContactCardComponent } from './my-contact-card.component';

describe('MyContactCardComponent', () => {
  let component: MyContactCardComponent;
  let fixture: ComponentFixture<MyContactCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyContactCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyContactCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
